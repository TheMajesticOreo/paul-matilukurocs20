import random
"""
 Pygame base template for opening a window

 Sample Python/Pygame Programs
 Simpson College Computer Science
 http://programarcadegames.com/
 http://simpson.edu/computer-science/

 Explanation video: http://youtu.be/vRB_983kUMc
"""

import pygame

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

pygame.init()

# Set the width and height of the screen [width, height]
SCREEN_HEIGHT = 720
SCREEN_WIDTH = 1280
size = (SCREEN_WIDTH, SCREEN_HEIGHT)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("My Game")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()
FLAKE_MAX_SIZE = 5
FLAKE_MIN_SIZE = 2
X = 0
Y = 1
SIZE = 2
snowList = []
SPEED = 3
FLAKES_MAX_SPEED = 5
FLAKES_MIN_SPEED = 2
colour = 4
PARTY = True

blockDrift[i] = x
test = GameObject(BLUE, 30, 30)
test.setVelocity(0, 3)
test.setPosition(x, 1)
print(x)

for i in range(500):
    snowFlake = []

    x = random.randrange(SCREEN_WIDTH)
    snowFlake.append(x)

    y = random.randrange(SCREEN_HEIGHT)
    snowFlake.append(y)

    size = random.randrange(FLAKE_MIN_SIZE, FLAKE_MAX_SIZE + 1)
    snowFlake.append(size)

    speed = random.randrange(FLAKES_MIN_SPEED, FLAKES_MAX_SPEED)
    snowFlake.append(speed)

    snowList.append(snowFlake)

    colour = WHITE

    if PARTY:
        colour = (random.randrange(256), random.randrange(256), random.randrange(256))
    snowFlake.append(colour)

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # --- Game logic should go here

    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.
    screen.fill(BLACK)

    # --- Drawing code should go here

    for i in range(len(snowList)):
        pygame.draw.circle(screen, WHITE, [snowList[i][X], snowList[i][Y]], snowList[i][SIZE])
        snowList[i][1] += 1

        if snowList[i][Y] - snowList[i][SIZE] > SCREEN_HEIGHT:
            y = random.randrange(-50, -10)
            snowList[i][Y] = y

            x = random.randrange(SCREEN_WIDTH + 1)
            snowList[i][X] = x

    pygame.display.flip()
    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
