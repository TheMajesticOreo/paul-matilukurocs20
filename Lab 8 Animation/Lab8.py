import random
"""
 Pygame base template for opening a window

 Sample Python/Pygame Programs
 Simpson College Computer Science
 http://programarcadegames.com/
 http://simpson.edu/computer-science/

 Explanation video: http://youtu.be/vRB_983kUMc
"""

import pygame

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

pygame.init()
SCREEN_WIDTH = 1280
SCREEN_HEIGHT = 720

# Set the width and height of the screen [width, height]
size = (1280, 720)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("My Game")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()
rectX = 50
rectY = 50
rectSpeedX = 5
rectSpeedY = 5
rectColour = 0
# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
    # --- Game logic should go here
    if rectY > 670 or rectY < 0:
        rectSpeedY = rectSpeedY * (-1)
        if rectY > 0:
            rectY += 2
        if rectY < 0:
            rectY -= 2

        rectColour = (random.randrange(256), random.randrange(256), random.randrange(256))
    if rectX > 1230 or rectX < 0:
        rectSpeedX = rectSpeedX * (-1)
        if rectX > 0:
            rectX += 2
        if rectX < 0:
            rectX -= 2
        rectColour = (random.randrange(256), random.randrange(256), random.randrange(256))

# --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.
    screen.fill(BLACK)

    # --- Drawing code should go here
    pygame.draw.rect(screen, rectColour, [rectX, rectY, 50, 50])
    rectX += rectSpeedX
    rectY += rectSpeedY
    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
