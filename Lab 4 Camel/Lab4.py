import random
print("Welcome to time travel")
print()
print("It is the year 2050, you are a scientist and you live in a zombie apocalypse world")
print("due to your research you have found the cure of the zombie outbreak")
print("but there is a problem")
print("80% of the population is already dead")
print("and the only way to fix this is to use the time machine that you built,")
print("in order to prevent the outbreak from occurring.")
print("The outbreak started in 2019 in a science lab at Dr martin Lebouldus high school")
print("You have gone back to 2019, but you have some problems,")
print("the time machine can only travel back to time but is not that precise for location.")
print("You have landed at forest and now you must get to the school which is 20 miles away.")
print()
print("GET TO THE SCHOOL BEFORE THE APOCALYPSE BEGINS!")
print()
print()
print("FACTORS TO KEEP IN MIND")
print()
print("You only have 360 minutes(6 hours) before the event occurs.")
print("You are 20 miles away from your objective.")
print("You only 2 packs of water and hotdogs.")
print("Wherever you consume one of your hotdog and water from the bottle, your stamina will be increased by 40.")
print("Whenever you rest, your stamina will be increased by 50 and you will lose 80 minutes.")
print("Every time you decide to run you will travel 2.5 miles, but you will lose 25 stamina and wasted 20 minutes")
print("Every time you decide to walk you will travel 4 miles, but you will lose 10 stamina and wasted 1 hour")

print()
print()
print()

time = 240
distance = 20
your_stamina = 100
supplies = 2

done = False
if time <= 0:
    print("You ran out of time, the zombie apocalypse has begun")
    done = True
while not done:
    print("A. rest")
    print("B. run to the objective")
    print("C. walk to the objective")
    print("D. use consumable")
    print("E. status check")
    print("Q. quit")
    print()

    bonus_event = random.randrange(1, 20)

# If Your Stamina...

    if your_stamina <= 0:
        print("Oh no, you were so close, but you died due to extreme fatigue")
        done = True
    if distance <= 0:
        print("Congratulations, you have said the world by avoiding the zombie apocalypse")
        print("You have won the game")
        done = True
    print()
    if bonus_event == 5:
        print()
    if supplies == 0:
        print("oh no, you ran out of supplies")
    if time <= 60:
        print("Hurry up!, you only have less than 1 hour left")
    answer = input("Enter your answer here: ")
    print()

# If Answer is A (Rest)

    if answer.lower() == "a":
        print("you rested for 20 minutes")
        your_stamina += 50
        time -= 80
        if your_stamina >= 51 <= 100:
            print("Your stamina is pretty decent right now")
            print()
        elif your_stamina <= 50 >= 25:
            print("Your stamina is less than 50 but above 25, remember to status check and regenerate your stamina")
            print()
        elif your_stamina <= 24 >= 1:
            print("You are getting very tired, your stamina is between 24 to 1. Regain your stamina before you die!")
        else:
            print("You died due to extreme fatigue from lack of stamina")
            done = True
    print()

# If Answer is B (Run)

    if answer.lower() == "b":
        distance -= 3
        print("you are", distance, "miles away from your objective")
        time -= 20
        your_stamina -= 25
        if your_stamina >= 51 <= 100:
            print("Your stamina is pretty decent right now")
            print()
        elif your_stamina >= 25 <= 50:
            print("Your stamina is less than 50 but above 25, remember to status check and regenerate your stamina")
            print()
        elif your_stamina >= 1 <= 24:
            print("You are getting very tired, your stamina is between 24 to 1.Regenerate your stamina before you die!")
            print()
        else:
            print("You died due to extreme fatigue from lack of stamina")
            done = True

# If Answer is C (Walk)

    if answer.lower() == "c":
        print("you have walked 3 miles")
        distance -= 4
        your_stamina -= 10
        time -= 60
        if your_stamina >= 51 <= 100:
            print("Your stamina is pretty decent right now")
            print()
        elif your_stamina <= 50 >= 25:
            print("Your stamina is less than 50 but above 25, remember to status check and regenerate your stamina")
            print()
        elif your_stamina <= 24 >= 1:
            print("You are getting very tired, your stamina is between 24 to 1.Regenerate your stamina before you die!")
            print()
        else:
            print("You died due to extreme fatigue from lack of stamina")
            done = True

# If Answer is D (Use Consumable)

    if answer.lower() == "d":
        print(" You have regained 60 stamina")
        your_stamina += 40
        time -= 30
        supplies -= 1

# If Answer is E (Status Check)

    if answer.lower() == "e":
        print("your time remaining is:", time, "minutes")
        print("your distance is:", distance, "miles")
        print("your supplies remaining are:", supplies)
        print("The amount of stamina remaining is:", your_stamina)
        print()
        print()
        time -= 5
        print("You have lost 5 minutes while checking.")
        print()

# If Answer is Q (Quit)

    if answer.lower() == "q":
        done = True
        print()
        print("You have quit the game.")
    print()
    print()
