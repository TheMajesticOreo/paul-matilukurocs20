"""
 Pygame base template for opening a window

 Sample Python/Pygame Programs
 Simpson College Computer Science
 http://programarcadegames.com/
 http://simpson.edu/computer-science/

 Explanation video: http://youtu.be/vRB_983kUMc
"""

import pygame
done = False

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
TEAL = (0, 233, 255)
GREY = (124, 121, 121)
BLUE = (0, 0, 255)
ORANGE = (255, 102, 0)
PINK = (255, 0, 199)
BROWN = (224, 163, 116)
pygame.init()

# Set the width and height of the screen [width, height]
size = (1280, 900)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("My Game")

# Loop until the user clicks the close button.


# Used to manage how fast the screen updates
clock = pygame.time.Clock()
orangeX = 239
orangeY = 661
speedY = 10.1
speedX = 3
handAnime = 750

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # --- Game logic should go here
    mousePosition = pygame.mouse.get_pos()
    mouseX = mousePosition[0]
    mouseY = mousePosition[1]
    print("(" + str(mouseX) + ", " + str(mouseY) + ") ")
    # codes for animating the ball
    pygame.draw.ellipse(screen, ORANGE, [orangeX, orangeY, 65, 65])
    orangeX += 3
    orangeY -= speedY
    speedY -= 0.08
    if orangeY > 775:
        speedY = 4
        shiny = [pygame.draw.ellipse(screen, BLUE, [orangeX, orangeY, 65, 65])]
    if orangeX > 1280:
        orangeX = 239
        orangeY = 661
        speedY = 10.1
        speedX = 3
        shiny = [pygame.draw.ellipse(screen, ORANGE, [orangeX, orangeY, 65, 65])]
        handAnime = 750
    if handAnime > 590:
        handAnime -= 8

    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.
    screen.fill(RED)

    # --- Drawing code should go here
    # variables for stuff..
    red = 0
    green = 255
    blue = 100
    y = 0
    for i in range(1000):
        myColour = (red, green, blue)
        pygame.draw.line(screen, myColour, [0, y], [1280, y], 5)
        y += 1
        red += 0
        green -= 0.2
        blue -= 0.085
    # code for the backboard
    pygame.draw.rect(screen, BLACK, [1000, 420, 280, 35])
    pygame.draw.rect(screen, GREY, [1000, 196, 20, 300])
    pygame.draw.rect(screen, ORANGE, [880, 375, 120, 10])
    pygame.draw.rect(screen, TEAL, [0, 829, 1280, 100])

    # Code to draw stephen curry
    pygame.draw.rect(screen, WHITE, [144, 755, 20, 75])
    pygame.draw.rect(screen, BLUE, [137, 700, 35, 70])
    pygame.draw.rect(screen, ORANGE, [132, 630, 45, 80])
    pygame.draw.ellipse(screen, BLUE, [149, 649, 20, 20])
    pygame.draw.line(screen, BROWN, [159, 659], [230, handAnime], 20)
    pygame.draw.rect(screen, PINK, [144, 816, 50, 15])
    pygame.draw.ellipse(screen, BLACK, [126, 570, 50, 51])
    pygame.draw.ellipse(screen, BROWN, [127, 580, 50, 51])
    pygame.draw.ellipse(screen, WHITE, [154, 589, 15, 15])
    pygame.draw.ellipse(screen, BLACK, [158, 591, 10, 10])
    pygame.draw.rect(screen, BLACK, [155, 610, 15, 5])

    # Loops for basketball nets
    x = 880
    for shape in range(5):
        pygame.draw.rect(screen, WHITE, [x + 20, 385, 5, 100])
        x += 20
    y = 390
    for vertically in range(5):
        pygame.draw.rect(screen, WHITE, [900, y + 15, 85, 5])
        y += 15
    # The code for the ball
    shiny = [pygame.draw.ellipse(screen, ORANGE, [orangeX, orangeY, 65, 65])]
    # text
    font = pygame.font.SysFont('Calibri', 60, True, False)
    text = font.render("#30, golden state warriors, STEPHEN CURRY!", True, BLACK)
    screen.blit(text, [103, 36])
    font = pygame.font.SysFont('Calibri', 45, True, False)
    text = font.render("Splash city", True, BLACK)
    screen.blit(text, [275, 847])
    font = pygame.font.SysFont('Calibri', 10, True, True)
    text = font.render("NIKE", True, BLACK)
    screen.blit(text, [156, 821])
    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
