numberPipes = 10


maxPipeGap = 150
minPipeGap = 50


maxPipeSeparation = 300
minPipeSeparation = 200

maxPipeWidth = 100
minPipeWidth = 20

pipeSpeed = -5

screenWidth = 1200
screenHeight = 720

# player variables

PLAYER_COLOUR = (0, 0, 0)
playerWidth = 20
playerHeight = 20
playerX = 20
playerY = 0
