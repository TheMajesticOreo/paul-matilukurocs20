
"""
 Pygame base template for opening a window

 Sample Python/Pygame Programs
 Simpson College Computer Science
 http://programarcadegames.com/
 http://simpson.edu/computer-science/

 Explanation video: http://youtu.be/vRB_983kUMc
"""
import random
from object import *
import c
import pygame

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLUE = (0, 97, 255)

pygame.init()

# Set the width and height of the screen [width, height]
size = (c.screenWidth, c.screenHeight)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("Floppy Square")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

player = GameObject(c.PLAYER_COLOUR, c.playerWidth, c.playerHeight)
player.setPosition(c.playerX, 200)

pipeList = []
pipeX = c.screenWidth / 2
for i in range(c.numberPipes):
    # horizontal distance
    gap = random.randrange(c.minPipeGap, c.maxPipeGap + 1)

    top = random.randrange(50, c.screenHeight - 100 - gap)

    separation = random.randrange(c.minPipeSeparation, c.maxPipeSeparation)

    width = random.randrange(c.minPipeWidth, c.maxPipeWidth)

    pipeX += separation

    topPipe = GameObject(RED, width, top)
    topPipe.setPosition(pipeX, 0)
    topPipe.setVelocity(c.pipeSpeed, 0)

    bottomPipe = GameObject(BLUE, width, c.screenHeight - top - gap)
    bottomPipe.setPosition(pipeX, top + gap)
    bottomPipe.setVelocity(c.pipeSpeed, 0)
    pipeList.append([topPipe, bottomPipe])
# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # --- Game logic should go here
    mousePosition = pygame.mouse.get_pos()
    player.setPosition(c.playerX, mousePosition[1])

    for pipe in pipeList:
        pipe[0].update()
        pipe[1].update()

    for pipe in pipeList:
        if player.ifCollide(pipe[0]):
            player.alive = False

        if player.ifCollide(pipe[1]):
            player.alive = False

    if not player.alive:
        c.player.colour = (255, 0, 0)
        for i in range(len(pipeList)):
            pipeList[i][0].setVelocity(0, 0)
            pipeList[i][0].setVelocity(0, 0)

    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.
    screen.fill(WHITE)

    # --- Drawing code should go here
    player.draw(screen)
    for pipe in pipeList:
        pipe[0].draw(screen)
        pipe[1].draw(screen)

    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
