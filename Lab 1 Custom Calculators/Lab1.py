# Fahrenheit to Celsius converter
# Changes Temperatures in Fahrenheit to Celsius
# Paul Matilukuro
# February 5th, 2019
# Computer Science 20

print("This program converts Fahrenheit to Celsius")
print("write to degree of the fahrenheit temperature below")
temperatureInFahrenheit = input("temperatureInFahrenheit: ")
temperatureInFahrenheit = float(temperatureInFahrenheit)

conversionToCelsius = ((temperatureInFahrenheit - 32) * (5/9))

print(conversionToCelsius)
print("This is your answer in Celsius")
