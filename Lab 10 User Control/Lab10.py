"""
 Pygame base template for opening a window

 Sample Python/Pygame Programs
 Simpson College Computer Science
 http://programarcadegames.com/
 http://simpson.edu/computer-science/

 Explanation video: http://youtu.be/vRB_983kUMc
"""

import pygame

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
TEAL = (0, 233, 255)
GREY = (124, 121, 121)
BLUE = (0, 0, 255)
ORANGE = (255, 102, 0)
PINK = (255, 0, 199)
BROWN = (224, 163, 116)
pygame.init()

# Code for drawing curry


def curry(screen, x, y):
    pygame.draw.rect(screen, WHITE, [144 - 126 + x, 755 - 570 + y, 20, 75])
    pygame.draw.rect(screen, BLUE, [137 - 126 + x, 700 - 570 + y, 35, 70])
    pygame.draw.rect(screen, ORANGE, [132 - 126 + x, 630 - 570 + y, 45, 80])
    pygame.draw.ellipse(screen, BLUE, [149 - 126 + x, 649 - 570 + y, 20, 20])
    pygame.draw.line(screen, BROWN, [159 - 126 + x, 659 - 570 + y], [206 - 126 + x, 600 - 570 + y], 20)
    pygame.draw.rect(screen, BROWN, [201 - 126 + x, 595 - 570 + y, 40, 15])
    pygame.draw.rect(screen, PINK, [144 - 126 + x, 816 - 570 + y, 50, 15])
    pygame.draw.ellipse(screen, BLACK, [126 - 126 + x, 570 - 570 + y, 50, 51])
    pygame.draw.ellipse(screen, BROWN, [127 - 126 + x, 580 - 570 + y, 50, 51])
    pygame.draw.ellipse(screen, WHITE, [154 - 126 + x, 589 - 570 + y, 15, 15])
    pygame.draw.ellipse(screen, BLACK, [158 - 126 + x, 591 - 570 + y, 10, 10])
    pygame.draw.rect(screen, BLACK, [155 - 126 + x, 610 - 570 + y, 15, 5])

# Code for ball


def ball(screen, x, y):
    pygame.draw.ellipse(screen, ORANGE, [100 - 100 + x, 100 - 100 + y, 65, 65])

# Drawing the Nets


def nets(screen):
    pygame.draw.rect(screen, BLACK, [1000, 420, 280, 35])
    pygame.draw.rect(screen, GREY, [1000, 196, 20, 300])
    pygame.draw.rect(screen, ORANGE, [880, 375, 120, 10])
    x = 880
    for shape in range(5):
        pygame.draw.rect(screen, WHITE, [x + 20, 385, 5, 100])
        x += 20
    y = 390
    for vertically in range(5):
        pygame.draw.rect(screen, WHITE, [900, y + 15, 85, 5])
        y += 15


# Set the width and height of the screen [width, height]
size = (1280, 900)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("My Game")

# Loop until the user clicks the close button.
done = False

xSpeed = 0
ySpeed = 0

xPosi = 100
yPosi = 100

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        elif event.type == pygame.KEYDOWN:
            # left
            if event.key == pygame.K_LEFT:
                xSpeed = -5

            # right
            elif event.key == pygame.K_RIGHT:
                xSpeed = 5

            # up
            elif event.key == pygame.K_UP:
                ySpeed = -5

            # down
            elif event.key == pygame.K_DOWN:
                ySpeed = 5
        # if a key isn't held down
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                xSpeed = 0
            elif event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                ySpeed = 0

    # --- Game logic should go here
    xPosi += xSpeed
    yPosi += ySpeed
    pos = pygame.mouse.get_pos()
    x = pos[0]
    y = pos[1]
    # wave... i mean off screen check
    if xPosi >= 1170:
        xPosi = 1170
    if xPosi <= 0:
        xPosi = 0
    if yPosi >= 640:
        yPosi = 640
    if yPosi <= 0:
        yPosi = 0
    if pos[0] >= 1170:
        x = 1170
    if pos[0] <= 0:
        x = 0
    if pos[1] >= 700:
        y = 700
    if pos[1] <= 0:
        y = 0
    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.
    screen.fill(WHITE)

    # --- Drawing code should go here
    red = 0
    green = 255
    blue = 100
    yeet = 0
    for i in range(1000):
        myColour = (red, green, blue)
        pygame.draw.line(screen, myColour, [0, yeet], [1280, yeet], 5)
        yeet += 1
        red += 0
        green -= 0.2
        blue -= 0.085
    # drawing code for the image
    curry(screen, xPosi, yPosi)
    ball(screen, pos[0], pos[1])
    nets(screen)

    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
