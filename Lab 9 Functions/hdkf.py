def min3(a, b, c):
    if a <= b and a <= c:
        return a
    elif b <= a and b <= c:
        return b
    elif c <= b and c <= a:
        return c


print(min3(4, 7, 5))
print(min3(4, 5, 5))
print(min3(4, 4, 4))
print(min3(-2, -6, -100))
print(min3("Z", "B", "A"))


def box(x, y):
    for row in range(x):
        print("*", end=" ")
    for coloumn in range(y):
        print("*", end=" ")


box(7, 5)  # Print a box 7 high, 5 across
print()   # Blank line
