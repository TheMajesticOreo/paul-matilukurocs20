import random


def min3(a, b, c):
    if a <= b and a <= c:
        return a
    if b <= a and b <= c:
        return b
    if c <= b and c <= a:
        return c


print(min3(4, 7, 5))
print(min3(4, 5, 5))
print(min3(4, 4, 4))
print(min3(-2, -6, -100))
print(min3("Z", "B", "A"))


def space(k):
    for i in range(k):
        print()


space(2)


def box(x, y):
    for row in range(x):
        for column in range(y):
            print("*", end=" ")
        print()


box(7, 5)  # Print a box 7 high, 5 across
print()   # Blank line
box(3, 2)  # Print a box 3 high, 2 across
print()   # Blank line
box(3, 10)  # Print a box 3 high, 10 across

space(2)

my_list = [36, 31, 79, 96, 36, 91, 77, 33, 19, 3, 34, 12, 70, 12, 54, 98, 86, 11, 17, 17]


def find(my_list, y):
    for i in range(len(my_list)):
        if my_list[i] == y:
            print("found", y, "at position", i)


find(my_list, 12)
find(my_list, 91)
find(my_list, 80)


space(2)

fourthList = []


def createalist(number):
    for every in range(number):
        store = random.randrange(6)
        store += 1
        fourthList.append(store)
    return fourthList


createalist(5)
print(fourthList)

space(2)


def countlist(count, freq):
    variable = 0
    for each in range(len(count)):
        if count[each] == freq:
            variable += 1
    print(variable)


countlist([1, 2, 3, 3, 3, 4, 2, 1], 3)


space(2)


def averagelist(avg):
    total = 0
    for every in range(len(avg)):
        total += avg[every]
    total /= len(avg)
    print(total)


averagelist([1, 2, 3])


space(2)


empty = []
for i in range(10000):
    numberOfOnes = 0
    numberOfTwos = 0
    numberOfThrees = 0
    numberOfFour = 0
    numberOfFive = 0
    numberOfSix = 0
    store = random.randrange(1, 7)
    empty.append(store)
countlist(empty, 1)
