# Joel Bae & Paul Matilukuro
# Computer Science 20
# Lab 7 - Adventure Game

# Set variables
room_list = []
current_room = 0
done = False
back_key = False
balcony_key = False

# room 0
room = ["You have entered the Main room, it is old, dirty, and dusty with the smell of pie."
        " \nYou see a room to your west, a room to the east and a flight of stairs up north.",
        8, 6, None, 1]

room_list.append(room)

# room 1
room = ["You have entered the west hall, it is dark and desolate."
        " \nYou hear the sound of a jack in the box."
        " \nThere is a door to the north and a door to the east.", 2, 0, None, None]

room_list.append(room)

# room 2
room = ["You are in a bedroom"
        " lined with dusty old trophies and pictures of young clowns."
        " \nThere is blood on the wall and you see a giant jack in the box hanging from the ceiling "
        "spinning in slow circles. \nThere is a set of double doors to the east,"
        " and there seems to be a door south of you", None, 3, 1, None]

room_list.append(room)

# room 3
room = ["You are in the north hallway"
        " lined with paintings of clowns whose eyes seem to always stare at you. "
        "\nThere is a door to the north, a door to the east, and a door to the west", 4, 7, None, 2]

room_list.append(room)

# room 4
room = ["You have entered what seems to be a huge dining hall with food that has recently been prepared.\nAlmost as if"
        " it was made for visitors, there are 6 seats on each side of the table. "
        "\nAt the end, there is a chair with spikes instead of cushions, there seems to be blood on the spikes."
        "\nThere is a small door to the north, it looks peculiar, "
        "and there is a stair case to the east. \nThere is a door to the south", 5, 9, 3, None]

room_list.append(room)

# room 5
room = ["You enter a dark room, a very dark room.\nAs your eyes adjust to the light, you begin to see that there"
        " is a figure on the floor.\nYou look at the figure and see that it is dead with a gunshot wound"
        " to the head and blood seeping out of it,\noddly enough, he resembles the "
        "picture of the man who murdered the clowns. Memories of screaming, blood and guilt seems to become vivid,"
        " as if repressed by a distant part of your memory. \nYou see a spinning hypnotic wheel, families begging you"
        " to have mercy. Tears begin to swell in your eyes but you don't really know why. \nYou look up and realize "
        "that there is a mirror in the room, not one that is distorted but normal.\nYou see your reflection for the "
        "first time and see that you resemble the man on the floor, except this time you look transparent, you"
        " look cold, YOU LOOK LIKE A GHOST.\nAs you look at yourself, you begin to realize what has happened, what you "
        "have done.  ", None, None, 4, None]


room_list.append(room)

# room 6
room = ["You have entered what seems to be a hall without any chairs.\nyou see a bunch of what seems to be human shaped"
        " figures hanging from the ceiling with ropes attached to their heads. "
        "\nYou are not sure if they are real people."
        "\nIt is too dark to see. There is a door to the north and a door to the west.",
        7, None, None, 0]

room_list.append(room)

# Room 7
room = ["You have entered a bedroom. \nFor some reason, there seems to be 12 distorting mirrors lined along the walls."
        "\nYou can't see your own reflection. The mirror is too distorted."
        "\nThere is a door to the west and to the south.", None, None, 6, 3]

room_list.append(room)

# Stair/Room 8
room = ["You are walking on a set of stairs. You can go north or south.",
        10, None, 0, None]

room_list.append(room)

# Stair/Room 9
room = ["You are walking on a set of dimly lit stairs. You can go east or west.",
        None, 11, None, 4]

room_list.append(room)

# Room 10
room = ["You are in a short hallway that seems empty, you are feeling nervous but you don't know why."
        "\nThere is a door to "
        "the north or you can take the stairs southwards that goes down to the first floor", 11, None, 8, None]

room_list.append(room)

# Room 11
room = ["You have entered a room with 12 beds. \nIt is very cold and you swear you can hear whispers of different "
        "voices. \nThe beds are neatly arranged like whoever lived in this house had never left. \nThere is a door to "
        "the east leading to the balcony, a door to the south, and a set of stairs to the west.", None, 12, 10, 9]

room_list.append(room)

# Room 12
room = ["You have entered a balcony and as you look out the balcony, you drop the key off the balcony."
        " \nYou notice there are 11 bodies on the ground,"
        " it seemed like they jumped off the balcony. \nThere is a new key and a letter with a picture of a young clown"
        " on it."
        " The letter reads,\n\"I asked them if i could join, they didn't allow me, "
        "so i hypnotized them to jump off the "
        "balcony.\nI invited their families over and murdered them by hanging. "
        "\nI feel so guilty, I just wanted to be the "
        "12th clown, I have to end my life, why did i kill them? \nI will be in my favourite room in the house,"
        " the room behind the dining room"
        " to spend my last minutes.\"\nThe key opens the room behind the dining room."
        "\nThere is a door to the west leading back the way you came.", None, None, None,
        11]

room_list.append(room)

# Game loop
while not done:

    # Normal game operations
    if current_room != 5:
        print()
        print(room_list[current_room][0])
        print()
        action = input("What will you do(n,e,s,w) Press q to quit: ")

        # Move north
        if action.lower() == "n":
            print()
            next_room = room_list[current_room][1]
            if next_room is None:
                print()
                print("You can't go that way!")

            # Back room access
            elif next_room == 5:
                if not back_key:
                    print()
                    print("That room is locked")
                if back_key:
                    print()
                    print("You unlocked the door")
                    current_room = next_room

            else:
                current_room = next_room

        # Move east
        elif action.lower() == "e":
            print()
            next_room = room_list[current_room][2]
            if next_room is None:
                print()
                print("You can't go that way!")

            # Balcony access
            elif next_room == 12:
                if balcony_key:
                    current_room = next_room
                    back_key = True
                    balcony_key = False
                elif not balcony_key:
                    print()
                    print("The door is locked")
            else:
                current_room = next_room

        # Move south
        elif action.lower() == "s":
            next_room = room_list[current_room][3]
            if next_room is None:
                print()
                print("You can't go that way!")
            else:
                current_room = next_room

        # Move west
        elif action.lower() == "w":
            next_room = room_list[current_room][4]
            if next_room is None:
                print()
                print("You can't go that way!")
            else:
                current_room = next_room

        # Quit game
        elif action.lower() == "q":
            print("You have quit the game...")
            done = True

        # Input error
        else:
            print()
            print("we don't understand that!")

    # End of game
    elif current_room == 5:
        print()
        print(room_list[current_room][0])
        print()
        print("T H E   E N D")
        done = True

    # Balcony key
    if current_room == 7 and not balcony_key and not back_key:
        balcony_key = True
        print()
        print("You have found a key that opens the balcony")
print()

