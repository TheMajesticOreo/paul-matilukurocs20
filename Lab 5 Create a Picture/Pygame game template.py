import math
import pygame
PI = math.pi
"""
 Pygame base template for opening a window

 Sample Python/Pygame Programs
 Simpson College Computer Science
 http://programarcadegames.com/
 http://simpson.edu/computer-science/

 Explanation video: http://youtu.be/vRB_983kUMc
 
"""

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (103, 229, 94)
RED = (255, 0, 0)
BLUE = (0, 14, 112)
PURPLE = (88, 9, 137)
BROWN = (132, 77, 0)
ORANGE = (196, 103, 21)
DARKGREEN = (25, 102, 0)
PINK = (255, 0, 199)
TEAL = (0, 255, 187)
pygame.init()

# Set the width and height of the screen [width, height]
size = (1280, 720)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("Chap 5 demo")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # --- Game logic should go here

    mousePosition = pygame.mouse.get_pos()
    mouseX = mousePosition[0]
    mouseY = mousePosition[1]

    print("(" + str(mouseX) + ", " + str(mouseY) + ") ")

    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.
    screen.fill(TEAL)

    # --- Drawing code should go here
    red = 0
    green = 255
    blue = 187
# A smart way to draw the gradient
    eye = 740
    y = 0
    for i in range(2):
        pygame.draw.ellipse(screen, WHITE, [eye, 229, 20, 20])
        eye += 44
    for i in range(1000):
        myColour = (red, green, blue)
        pygame.draw.line(screen, myColour, [0, y], [1280, y], 5)
        y += 1
        green -= 0.2
# these are the shapes that are used to draw the figures
    pygame.draw.line(screen, BROWN, [561, 226], [636, 298], 31)
    pygame.draw.rect(screen, BROWN, [636, 287, 80, 30])
    pygame.draw.rect(screen, WHITE, [715, 288, 120, 175])
    pygame.draw.line(screen, WHITE, [734, 464], [738, 522], 40)
    pygame.draw.line(screen, WHITE, [806, 462], [809, 521], 40)
    pygame.draw.rect(screen, DARKGREEN, [714, 445, 122, 18])
    pygame.draw.ellipse(screen, ORANGE, [465, 132, 100, 100])
    pygame.draw.line(screen, BROWN, [740, 522], [785, 590], 39)
    pygame.draw.line(screen, BROWN, [810, 519], [859, 572], 40)
    pygame.draw.rect(screen, GREEN, [741, 325, 10, 45])
    pygame.draw.rect(screen, GREEN, [760, 325, 10, 45])
    pygame.draw.rect(screen, BROWN, [752, 288, 50, 20])
    pygame.draw.line(screen, BLACK, [783, 593], [749, 634], 37)
    pygame.draw.line(screen, BLACK, [856, 575], [827, 619], 37)
    pygame.draw.rect(screen, WHITE, [758, 450, 10, 5])
    pygame.draw.rect(screen, WHITE, [774, 450, 10, 5])
    pygame.draw.rect(screen, BROWN, [530, 233, 50, 20])
    pygame.draw.rect(screen, BROWN, [833, 288, 80, 30])
    pygame.draw.rect(screen, BROWN, [913, 289, 30, 90])
    pygame.draw.ellipse(screen, BROWN, [722, 196, 100, 100])
    pygame.draw.ellipse(screen, BLACK, [763, 257, 25, 25])
# loops for the eyes, to reduce work load
    eye = 740
    y = 0
    for i in range(2):
        pygame.draw.ellipse(screen, WHITE, [eye, 229, 20, 20])
        eye += 44

    blackEye = 744
    y = 0
    for i in range(2):
        pygame.draw.ellipse(screen, BLACK, [blackEye, 229, 15, 12])
        blackEye += 44

    font = pygame.font.SysFont('Calibri', 30, True, False)
    text = font.render("YOU REACH, I TEACH - UNCLE DREW", True, RED)
    screen.blit(text, [99, 99])
    text = font.render("KYRIE", True, GREEN)
    screen.blit(text, [734, 399])
    font = pygame.font.SysFont('Calibri', 85, True, False)
    text = font.render("#CELTICS NATION", True, BLACK)
    screen.blit(text, [88, 472])
    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
