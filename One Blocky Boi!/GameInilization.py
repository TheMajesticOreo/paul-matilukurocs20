"""
 Pygame base template for opening a window

 Sample Python/Pygame Programs
 Simpson College Computer Science
 http://programarcadegames.com/
 http://simpson.edu/computer-science/

 Explanation video: http://youtu.be/vRB_983kUMc
"""
import random
from papi import *
import s
import pygame

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
TEAL = (0, 233, 255)
GREY = (124, 121, 121)
BLUE = (0, 0, 255)
ORANGE = (255, 102, 0)
PINK = (255, 0, 199)
BROWN = (224, 163, 116)
BRIGHT_GREEN = (12, 255, 0)

pygame.init()

# Set the width and height of the screen [width, height]
size = (1200, 720)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("Blocky Boi")

# Loop until the user clicks the close button.
playerList = []
associate = []
s.done = False

amount = 25
y = 1
set = 0

for i in range(amount):

    test = GameObject(BLUE, 30, 30)
    omg = random.randrange(1, 1200)
    y = random.randrange(1, 400)

    test.setPosition(omg, y)
    test.setVelocity(0, random.randrange(1, 4))

    playerList.append(test)
    associate.append(y)
    set += 1
if set >= 22:
    for i in range(amount):
        test = GameObject(BLUE, 30, 30)
        omg = random.randrange(1, 1200)
        y = random.randrange(1, 400)

        test.setPosition(omg, y)
        test.setVelocity(0, random.randrange(1, 4))

        playerList.append(test)
        associate.append(y)
        set += 1
        set = 0

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

xPos = 600
yPos = 600
blockSpeedX = 0
blockSpeedY = 0
oof = 0

player = GameObject(BRIGHT_GREEN, 10, 10)
pygame.mouse.set_visible(False)
background_image = pygame.image.load("Goat.jpg").convert()
# -------- Main Program Loop -----------
while not s.done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            s.done = True
        elif event.type == pygame.KEYDOWN:
            # left
            if event.key == pygame.K_LEFT:
                blockSpeedX = -3

            # right
            elif event.key == pygame.K_RIGHT:
                blockSpeedX = 3

            # up
            elif event.key == pygame.K_UP:
                blockSpeedY = -3

            # down
            elif event.key == pygame.K_DOWN:
                blockSpeedY = 3
        # if a key isn't held down
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                blockSpeedX = 0
            elif event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                blockSpeedY = 0
    # --- Game logic should go here
    mousePosition = pygame.mouse.get_pos()

    xPos += blockSpeedX
    yPos += blockSpeedY
    i = 255

    for test in playerList:
        test.update()
    for test in playerList:
        if player.ifCollide(test):
            player.alive = False
            i = random.randrange(1, 255)
            j = random.randrange(1, 255)
            k = random.randrange(1, 255)
            PINK = [j, i, k]
            player.colour = PINK
            oof += 1
        player.death(test)
    if yPos == 0:
        s.done = True
        print("You Won")
    # screen checks for the player object
    if xPos < 0:
        xPos = 0
    if xPos > 1190:
        xPos = 1190
    if yPos < 0:
        yPos = 0
    if yPos > 710:
        yPos = 710
    player.setPosition(xPos, yPos)
    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.
    screen.fill(WHITE)

    # --- Drawing code should go here

    # Gradient Lines
    red = 0
    green = 255
    blue = 187
    y = 0
    for i in range(1000):
        myColour = (red, green, blue)
        pygame.draw.line(screen, myColour, [0, y], [1280, y], 5)
        y += 1
        green -= 0.2
    # image
    screen.blit(background_image, [0, 0])
    # Code to draw the "BLOCKY BOI'S"
    for test in playerList:
        test.draw(screen)
    player.draw(screen)
    # text
    font = pygame.font.SysFont('Calibri', 35, True, False)
    text = font.render("Level 1", True, BLACK)
    screen.blit(text, [90, 36])

    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
