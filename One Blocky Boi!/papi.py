import pygame
import random
import s


class GameObject:

    def __init__(self, colour, width, height):
        self.colour = colour
        self.image = pygame.Surface([width, height])
        self.image.fill(colour)
        self.oof = 0
        self.rect = self.image.get_rect()

        self.vX = random.randrange(1, 100)
        self.vY = random.randrange(1, 100)

        self.alive = True

    def draw(self, screen):
        pygame.draw.rect(screen, self.colour, self.rect)

    def setVelocity(self, vX, vY):
        self.vX = vX
        self.vY = vY

    def update(self):
        self.rect.x += self.vX
        self.rect.y += self.vY
        if self.rect.y > 720:
            self.rect.y = -30
            self.rect.x = random.randrange(1200)
            self.vY += 1

    def setPosition(self, x, y):
        self.rect.x = x
        self.rect.y = y

    def ifCollide(self, target):
        if self.rect.colliderect(target.rect):
            return True
        return False

    def death(self, target):
        if self.rect.colliderect(target.rect):
            self.oof += 1
        if self.oof == 15:
            print("over")
            s.done = True
