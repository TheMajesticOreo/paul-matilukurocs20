# Mad Libs

# Change the following at your own risk

import random

# Fill list of adjectives

adjectiveList = []
file = open("adjectives.dat")
 
for line in file:
    line = line.strip()
    adjectiveList.append(line)

file.close()

# Fill list of nouns

nounList = []
file = open("nouns.dat")
 
for line in file:
    line = line.strip()
    nounList.append(line)

# Fill list of verbs

verbList = []
file = open("verbs.dat")
 
for line in file:
    line = line.strip()
    verbList.append(line)
file.close()

# Start working below

# Exercise 1
# Have the computer generate a MadLib of the form:
# The <noun> <verb> after the <adjective> <adjective> <noun> while the <noun> <verb> the <noun>
# Do not just print the MadLib - store it in a string!
file = open("madlibs.dat", "w")
for i in range(10):
    veryNice = ""
    veryNice += " The "
    veryNice += nounList[random.randrange(19)]
    veryNice += " "
    veryNice += verbList[random.randrange(13)]
    veryNice += " after the "
    veryNice += adjectiveList[random.randrange(16)]
    veryNice += " "
    veryNice += adjectiveList[random.randrange(16)]
    veryNice += " "
    veryNice += nounList[random.randrange(19)]
    veryNice += " While the "
    veryNice += nounList[random.randrange(19)]
    veryNice += " "
    veryNice += verbList[random.randrange(13)]
    veryNice += " the "
    veryNice += nounList[random.randrange(19)]
    veryNice += "\n"
    file.write(veryNice)
# Exercise 2
# The code below writes a single line to a file.  Modify it so that it writes ten randomly generated MadLibs to
# the file.


content = "This is what will be written into the file."


file.close()

# Exercise 3
